#!/usr/bin/env fennel
(local bots {})
(local projname "name")
(local tgt (.. "~/fennelbin/bin/" projname))
(local tgtlicense (.. "~/fennelbin/licenses/" projname ".org"))


(λ exec [cmd ?sep]
  "
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument
the default separator is ' ' (a single space) so you dont have to pad the strings"
  (let [sep (or ?sep " ")]
	(match (type cmd)
	  :table	(os.execute (tcat cmd sep))
	  :string	(os.execute cmd)
	  )))



(local proj {})
(set proj.name	"crow")
(set proj.code	
	 {:src	(.. bots.proj.name)
	  :tgt	(.. bots.proj.name)
	  })
(set proj.license
	 {:name "Simple Public License 2.0"
	  :src	"./LICENSE.org"
	  :tgt	""
	  })

(tset bots.proj		proj)
(tset bots.project	proj)



(fn bots.install [self]
  (exec ["install " "--compare " (.. projname ".fnl") tgt])
  (exec ["install " "--compare " "LICENSE.org" tgtlicense])
  (print "done")
  )


(fn bots.uninstall [self]
  (exec ["rm" "-f " tgt])
  (exec ["rm" "-f " tgtlicense])
  (print "done")
  )


bots
