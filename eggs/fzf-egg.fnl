#!/usr/bin/env fennel

(local fenv-rc
	   {:claws		false
		:eggs		false
		:feathers	false
		})
((require :fenv) fenv-rc)
(local utils (require :utils))
(local tcat table.concat)


(local go {})
(local egg {:name "fzf" :version "latest"
			:repo "github.com/junegunn/fzf"})

(fn go.install [{: repo : version}]
  (local gopath "/home/erik/raven/go-packages/bin")
  (utils.exec-print [(.. "GOBIN=" gopath " ") "go" "install" "-x" (.. repo "@" version)])
  )

;;(go.install egg)


