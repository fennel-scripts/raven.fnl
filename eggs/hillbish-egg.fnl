#!/usr/bin/env fennel

(local rc
	   {:claws		false
		:eggs		false
		:feathers	false
		})
((require :fenv) rc)

(local utils (require :utils))
(local tcat table.concat)
;;https://github.com/Rosettea/Hilbish/releases/download/v2.1.2/hilbish-v2.1.2-linux-amd64.tar.gz
(local egg
	   {:name "hilbish"
		:version "v2.1.2"}
	   )
(local meta {})
(local repo {})
(set repo.user "rosettea")
(set repo.name egg.name)

(local urls {})
(fn urls.gh [{: user : name }]
  (tcat ["https://github.com/" user "/" name "/"])
  )
;;(fn urls.gh-release [version ])

(set egg.baseurl (urls.gh repo))
(fn egg.download []
  (print (tcat
		  [egg.baseurl "releases/download/" egg.version
		   "hilbish-" egg.version "-linux-amd64.tar.gz"]
		  )))

(fn meta.__call []
  (print egg.baseurl)
  (egg.download)
  )


(setmetatable egg meta)

egg
