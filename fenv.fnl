#!/usr/bin/env fennel
;;#!/home/erik/raven/tree/bin/fennel
;;; NOTICE
;;IMPORTANT!
;;this file is designed to be imported rather than included
;;try to NOT use all the good variable names in the global-ish namespace, instead use let.
;;or if you have to use local, you should use a prefix souch as fenv-
;;; enviroment
;;(local (fenv-claws fenv-eggs fenv-feathers) (values true true true))

;;; code

;;(print package.path)
(fn [rc]

  (let [fennel	(require :fennel)		; TODO implement config file stuff
		home		(or (os.getenv "HOME") "/home/erik/")
		path		{:fennel	["./?.fnl" "./?/init.fnl"]; the default values for the enviroment variables
					 :lua		["./?.lua" "./?/init.lua"]}
		basedir	(.. home "/raven")
		;;(local confpath (or (os.getenv :XDG_CONFIG_HOME) (.. home "/.config")))
		;;(local libPath (.. home "/.local/fennel/lib/"))
		fenv
		{:claws		(or rc.claws	false)
		 :eggs		(or rc.eggs		false)
		 :feathers	(or rc.feathers	false)}
		]
	
	


	
	(table.insert (or package.loaders package.searchers) fennel.searcher)
	(table.insert path.lua (.. basedir "/tree/share/lua/5.4/?.lua"))
	(table.insert path.lua (.. basedir "/tree/share/lua/5.4/?/init.lua"))
	
	(table.insert path.fennel (.. basedir "/lib/?.fnl"))
	(table.insert path.fennel (.. basedir "/?.fnl"))
	;;(table.insert path.fennel (.. basedir "/lib/?/?.fnl"))
	;;(table.insert path.fennel (.. basedir "/lib/?/init.fnl"))
	
	(when fenv.eggs
	  (table.insert path.fennel (.. basedir "/eggs/?.fnl"))
	  (table.insert path.fennel (.. basedir "/eggs/?/?.fnl"))
	  (table.insert path.fennel (.. basedir "/eggs/?/init.fnl"))
	  (table.insert path.fennel (.. basedir "/eggs/?/egg.fnl"))
	  )

	(when fenv.claws
	  (table.insert path.fennel (.. basedir "/claws/?.fnl"))
	  (table.insert path.fennel (.. basedir "/claws/?/?.fnl"))
	  (table.insert path.fennel (.. basedir "/claws/?/init.fnl"))
	  (table.insert path.fennel (.. basedir "/claws/?/claw.fnl"))
	  )

	(when fenv.feathers
	  (table.insert path.fennel (.. basedir "/feathers/?.fnl"))
	  (table.insert path.fennel (.. basedir "/feathers/?/?.fnl"))
	  (table.insert path.fennel (.. basedir "/feathers/?/init.fnl"))
	  (table.insert path.fennel (.. basedir "/feathers/?/feather.fnl"))
	  )

	(set fennel.path (table.concat path.fennel ";"))
	(set package.path (table.concat path.lua ";"))
	path.fennel
	)

  ;;(print fennel.path)
  ;;(print package.path)
  )

;;; fenv.fnl ends here
