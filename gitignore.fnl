#!/usr/bin/env fennel


(local ignore
	   [
		"tree/*"
		".#*"
		])


(each [k v (ipairs ignore)] (print v))
