#!/usr/bin/env fennel

(local utils {})
(local tcat table.concat)
(set utils.home (or (os.getenv "HOME") "/home/erik/"))
(set utils.tcat tcat)


(λ utils.exec-print [cmd ?sep]
  "
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument (only used if provided a table)
the default separator is ' ' (a single space) so you dont have to pad the strings"
  (let [sep (or ?sep " ")]
	(match (type cmd)
	  :table	(print (tcat cmd sep))
	  :string	(print cmd)
	  )))


(λ utils.exec [cmd ?sep]
  "
execute system command defined in the first argument(string or squential table) and
an optional separator as the second argument (only used if provided a table)
the default separator is ' ' (a single space) so you dont have to pad the strings"
  (let [sep (or ?sep " ")]
	(match (type cmd)
	  :table	(os.execute (tcat cmd sep))
	  :string	(os.execute cmd)
	  )))

(fn utils.pp [...]
  (let [fennel (require :fennel)]
	(print (fennel.view ...))))


;;(table.insert utils :home homedir)

utils
